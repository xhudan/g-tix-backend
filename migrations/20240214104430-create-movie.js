'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poster: {
        type: Sequelize.TEXT
      },
      title: {
        type: Sequelize.STRING
      },
      director: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      writer: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      actors: {
        type: Sequelize.ARRAY(Sequelize.TEXT)
      },
      language: {
        type: Sequelize.STRING
      },
      duration: {
        type: Sequelize.TIME
      },
      age: {
        type: Sequelize.INTEGER
      },
      genre: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      synopsis: {
        type: Sequelize.TEXT
      },
      showtime_list: {
        type: Sequelize.ARRAY(Sequelize.TIME)
      },
      start_date: {
        type: Sequelize.DATEONLY
      },
      expired: {
        type: Sequelize.DATEONLY
      },
      price: {
        type: Sequelize.DOUBLE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('movies');
  }
};