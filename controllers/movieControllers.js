const { movie, book } = require("../models");
const { Op } = require("sequelize");
const catchAsync = require("../utils/catchAsync");
const { uploadToImagekit } = require("../lib/imagekit");
const tf = require('@tensorflow/tfjs-node');
const { getModel, getGenres, getAvgTicketsPerDay, getMaxTicketsSold, preprocessText, correctTypo } = require('./trainModel');

const getAllMovies = catchAsync(async (req, res) => {
    await movie
        .findAll()
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getMovies = catchAsync(async (req, res) => {
    const now = new Date().toLocaleDateString('en-CA');
    await movie
        .findAll({
            where: {
                expired: { [Op.gte]: now }
            },
            attributes: ['id', 'title', 'poster', 'age', 'start_date', 'expired'],
        })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getMovieByPK = catchAsync(async (req, res) => {
    const { movieId } = req.params;

    await movie
        .findByPk(movieId)
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
})

const updateMovie = catchAsync(async (req, res) => {
    const { movieId } = req.params;
    const { title, director, writer, actors, language, synopsis, genre, age, duration, start_date, expired, price, showtime_list } = req.body;

    // Convert single element array with empty string to null
    const convertEmptyStringArrayToNull = (value) => {
        if (value === "null" || value === "" || value[0] === "") {
            return null;
        }
        return value;
    };

    // Apply conversion to all fields using a loop
    const fieldsToUpdate = { title: title?.trim(), director, writer, actors, language: language?.trim(), synopsis: synopsis?.trim(), genre, age: age?.trim(), duration: duration?.trim(), start_date: start_date?.trim(), expired: expired?.trim(), price: price?.trim(), showtime_list };
    Object.keys(fieldsToUpdate).forEach(key => {
        fieldsToUpdate[key] = convertEmptyStringArrayToNull(fieldsToUpdate[key]);
    });

    if (Object.values(fieldsToUpdate).every(value => value === null)) {
        return res.status(400).json({ msg: "Missing or invalid variable!" });
    }

    if (!fieldsToUpdate.title.trim() || !fieldsToUpdate.genre || !fieldsToUpdate.start_date || !fieldsToUpdate.expired) {
        return res.status(400).json({ msg: "Please fill out all required fields!" });
    }

    let updateImage;
    if (req.file) {
        if (req.file.size > 5000000) {
            return res.status(400).json({
                status: "failed",
                message: "Image size must be under 5MB",
            });
        }
        const img = await uploadToImagekit(req);
        updateImage = img.url;
    }

    await movie
        .update({ ...fieldsToUpdate, poster: updateImage }, { where: { id: movieId }, returning: true })
        .then(([rowsUpdated, [updatedMovie]]) => {
            if (rowsUpdated === 0) {
                res.status(404).json({ msg: "Movie not found" });
            } else {
                res.status(200).json({ msg: "Movie has been updated successfully", data: updatedMovie });
            }
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const deleteMovie = catchAsync(async (req, res) => {
    const { movieId } = req.params;

    await movie
        .destroy({ where: { id: movieId } })
        .then(rowsDeleted => {
            if (rowsDeleted === 0) {
                res.status(404).json({ msg: "Movie not found" });
            } else {
                res.status(200).json({ msg: "Movie has been deleted successfully" });
            }
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getRecommendationMovies = catchAsync(async (req, res) => {
    try {
        const userPaidBookings = await book.findAll({
            where: { user_id: req.user.id, payment_status: "Issued" },
            attributes: ['movie_id'],
            include: [{ model: movie }],
        });

        const userPreferences = [];
        for (const booking of userPaidBookings) {
            const movieData = booking.movie;
            userPreferences.push({
                id: movieData.id,
                genre: movieData.genre || [],
                director: movieData.director || [],
                actors: movieData.actors || [],
                writer: movieData.writer || [],
                language: movieData.language || '',
            });
        }

        const movieIdsWithSimilarity = [];
        const now = new Date().toLocaleDateString("en-CA");

        for (const preference of userPreferences) {
            const similarMovies = await movie.findAll({
                where: {
                    [Op.and]: [
                        { start_date: { [Op.lte]: now } },
                        { expired: { [Op.gte]: now } },
                        { id: { [Op.notIn]: userPaidBookings.map(booking => booking.movie_id) } },
                    ],
                    [Op.or]: [
                        { genre: { [Op.overlap]: preference.genre } },
                        { director: { [Op.overlap]: preference.director } },
                        { actors: { [Op.overlap]: preference.actors } },
                        { writer: { [Op.overlap]: preference.writer } },
                        { language: preference.language },
                    ],
                },
                attributes: ['id', 'genre', 'director', 'actors', 'writer', 'language'],
            });

            const similarityScores = similarMovies.map(similarMovie => {
                const sharedAttributes = [
                    similarMovie.genre?.some(g => preference.genre.includes(g)) ? 1 : 0,
                    similarMovie.director?.some(d => preference.director.includes(d)) ? 1 : 0,
                    similarMovie.actors?.some(a => preference.actors.includes(a)) ? 1 : 0,
                    similarMovie.writer?.some(w => preference.writer.includes(w)) ? 1 : 0,
                    similarMovie.language === preference.language ? 1 : 0,
                ];

                const keywords = [];
                if (sharedAttributes[0]) keywords.push(...similarMovie.genre.filter(g => preference.genre.includes(g)));
                if (sharedAttributes[1]) keywords.push(...similarMovie.director.filter(d => preference.director.includes(d)));
                if (sharedAttributes[2]) keywords.push(...similarMovie.actors.filter(a => preference.actors.includes(a)));
                if (sharedAttributes[3]) keywords.push(...similarMovie.writer.filter(w => preference.writer.includes(w)));
                if (sharedAttributes[4] && similarMovie.language === preference.language) keywords.push(similarMovie.language);

                return { totalSimilarity: sharedAttributes.reduce((acc, cur) => acc + cur, 0), movieId: similarMovie.id, keywords };
            });

            const relateMovieId = preference.id;

            for (let i = 0; i < similarMovies.length; i++) {
                const { movieId, totalSimilarity, keywords } = similarityScores[i];
                const existingIndex = movieIdsWithSimilarity.findIndex(item => item.movieId === movieId);
                if (existingIndex !== -1) {
                    movieIdsWithSimilarity[existingIndex].totalSimilarity += totalSimilarity;
                    movieIdsWithSimilarity[existingIndex].keywords = Array.from(new Set([...movieIdsWithSimilarity[existingIndex].keywords, ...keywords]));
                    movieIdsWithSimilarity[existingIndex].relateMovieId.push(relateMovieId);
                } else {
                    movieIdsWithSimilarity.push({ totalSimilarity, movieId, keywords: Array.from(new Set(keywords)), relateMovieId: [relateMovieId] });
                }
            }
        }

        const recommendedMovies = await movie.findAll({
            where: { id: movieIdsWithSimilarity.map(movieWithSimilarity => movieWithSimilarity.movieId) },
            attributes: ['id', 'title', 'poster', 'age', 'start_date', 'expired'],
        });

        const sortedRecommendedMovies = recommendedMovies.sort((a, b) => {
            const movieA = movieIdsWithSimilarity.find(item => item.movieId === a.id);
            const movieB = movieIdsWithSimilarity.find(item => item.movieId === b.id);
            return movieB.totalSimilarity - movieA.totalSimilarity;
        });

        res.status(200).json({ movieIdsWithSimilarity, recommendedMovies: sortedRecommendedMovies });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

const addMovie = catchAsync(async (req, res) => {
    const { title, director, writer, actors, language, synopsis, genre, age, duration, start_date, price, showtime_list } = req.body;

    if (!title.trim() || !genre || !start_date) {
        return res.status(400).json({ msg: "Please fill out all required fields!" });
    }

    let insertImage;
    if (req.file) {
        if (req.file.size > 5000000) {
            return res.status(400).json({
                status: "failed",
                message: "Image size must be under 5MB!",
            });
        }
        const img = await uploadToImagekit(req);
        insertImage = img.url;
    }

    try {
        const model = getModel();
        const genres = getGenres();
        const maxTicketsSold = getMaxTicketsSold();
        const avgTicketsPerDay = getAvgTicketsPerDay();

        const genreArray = Array.isArray(genre) ? genre : (genre ? genre.split(',').map(g => g.trim()) : []);

        // Normalize received genres
        let normalizedGenres = genreArray.map(g => preprocessText(g).join(' '));
        normalizedGenres = normalizedGenres.join(' ').split(' '); // Split based on spaces to handle multiple genres

        // Correct typos with a threshold
        const correctedGenres = normalizedGenres.map(g => correctTypo(g, genres) || g);

        // Create input sequence for LSTM
        const sequenceLength = 5; // This should match the sequence length used in training

        // Calculate predictions for each corrected genre
        const genrePredictions = correctedGenres.map(g => {
            const inputSequence = new Array(sequenceLength).fill(new Array(genres.length).fill(0));
            const lastEntry = genres.map(genre => genre === g ? 1 : 0);
            inputSequence[sequenceLength - 1] = lastEntry;
            const inputTensor = tf.tensor3d([inputSequence]);
            const prediction = model.predict(inputTensor);
            return prediction.dataSync()[0] * maxTicketsSold;
        });

        // Find the best genre and its prediction
        const maxPrediction = Math.max(...genrePredictions);
        const indexOfMaxPrediction = genrePredictions.indexOf(maxPrediction);
        const bestGenre = correctedGenres[indexOfMaxPrediction];

        // Calculate predicted days based on the best prediction
        const predictedDays = Math.min(90, Math.max(7, Math.ceil(maxPrediction / avgTicketsPerDay)));

        const expired_date = new Date(start_date);
        expired_date.setDate(expired_date.getDate() + predictedDays);

        console.log("Normalized Received Genres:", normalizedGenres);
        console.log("Corrected Genres:", correctedGenres);
        console.log("Genres List from Model:", genres);
        console.log("Unscaled Genre Predictions (Tickets Sold):", genrePredictions);
        console.log("Best Genre for Prediction:", bestGenre);
        // console.log("Unscaled Prediction (Tickets Sold):", maxPrediction);
        console.log("Predicted Days:", predictedDays);

        const movieAdded = await movie.create({
            poster: insertImage,
            title: title.trim(),
            director,
            writer,
            actors,
            language: language ? language.trim() : null,
            synopsis: synopsis ? synopsis.trim() : null,
            genre,
            age: age ? age.trim() : null,
            duration: duration ? duration.trim() : null,
            start_date: start_date ? start_date.trim() : null,
            expired: expired_date,
            price: price ? price.trim() : null,
            showtime_list
        });

        return res.status(200).json({
            msg: "Movie has been created successfully",
            movieAdded, normalizedGenres, correctedGenres, genres, genrePredictions, bestGenre, predictedDays
        });
    } catch (error) {
        return res.status(error.statusCode || 500).json({ msg: error.message });
    }
});

module.exports = {
    getAllMovies,
    getMovies,
    getMovieByPK,
    addMovie,
    updateMovie,
    deleteMovie,
    getRecommendationMovies
};

// // Fully Connected Neural Network architecture
// const addMovie = catchAsync(async (req, res) => {
//     const { title, director, writer, actors, language, synopsis, genre, age, duration, start_date, price, showtime_list } = req.body;

//     if (!title.trim() || !genre || !start_date) {
//         return res.status(400).json({ msg: "Please fill out all required fields!" });
//     }

//     let insertImage;
//     if (req.file) {
//         if (req.file.size > 5000000) {
//             return res.status(400).json({
//                 status: "failed",
//                 message: "Image size must be under 5MB!",
//             });
//         }
//         const img = await uploadToImagekit(req);
//         insertImage = img.url;
//     }

//     try {
//         const model = getModel();
//         const genres = getGenres();
//         const maxTicketsSold = getMaxTicketsSold();
//         const avgTicketsPerDay = getAvgTicketsPerDay();

//         const genreArray = Array.isArray(genre) ? genre : (genre ? genre.split(',').map(g => g.trim()) : []);

//         // Normalize received genres
//         let normalizedGenres = genreArray.map(g => preprocessText(g).join(' '));
//         normalizedGenres = normalizedGenres.join(' ').split(' '); // Split based on spaces to handle multiple genres

//         // Koreksi typo dengan ambang batas
//         const correctedGenres = normalizedGenres.map(g => correctTypo(g, genres) || g);

//         // Menghitung prediksi tiket untuk setiap genre
//         const genrePredictions = correctedGenres.map(g => {
//             const inputGenreOneHot = genres.map(genre => genre === g ? 1 : 0);
//             const inputTensor = tf.tensor2d([inputGenreOneHot], [1, inputGenreOneHot.length]);
//             const prediction = model.predict(inputTensor).dataSync()[0];
//             return prediction * maxTicketsSold;
//         });

//         // Ambil prediksi tertinggi
//         const maxPrediction = Math.max(...genrePredictions);
//         const indexOfMaxPrediction = genrePredictions.indexOf(maxPrediction);
//         const bestGenre = correctedGenres[indexOfMaxPrediction];

//         // Hitung prediksi hari
//         const predictedDays = Math.min(90, Math.max(7, Math.ceil(maxPrediction / avgTicketsPerDay)));

//         const expired_date = new Date(start_date);
//         expired_date.setDate(expired_date.getDate() + predictedDays);

//         console.log("Normalized Received Genres:", normalizedGenres);
//         console.log("Corrected Genres:", correctedGenres);
//         console.log("Genres List from Model:", genres);
//         console.log("Unscaled Genre Predictions (Tickets Sold):", genrePredictions);
//         console.log("Best Genre for Prediction:", bestGenre);
//         // console.log("Unscaled Prediction (Tickets Sold):", maxPrediction);
//         console.log("Predicted Days:", predictedDays);

//         const movieAdded = await movie.create({
//             poster: insertImage,
//             title: title.trim(),
//             director,
//             writer,
//             actors,
//             language: language ? language.trim() : null,
//             synopsis: synopsis ? synopsis.trim() : null,
//             genre,
//             age: age ? age.trim() : null,
//             duration: duration ? duration.trim() : null,
//             start_date: start_date ? start_date.trim() : null,
//             expired: expired_date,
//             price: price ? price.trim() : null,
//             showtime_list
//         });

//         return res.status(200).json({
//             msg: "Movie has been created successfully",
//             movieAdded, normalizedGenres, correctedGenres, genres, genrePredictions, bestGenre, predictedDays
//         });
//     } catch (error) {
//         return res.status(error.statusCode || 500).json({ msg: error.message });
//     }
// });
