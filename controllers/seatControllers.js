const { seat, cinema, movie, book } = require("../models/");
const catchAsync = require("../utils/catchAsync");
const { Op } = require("sequelize");

const checkSeat = catchAsync(async (req, res) => {
    const { movieId } = req.params;
    const { cid, d, t } = req.query;

    const foundMovie = await movie.findByPk(movieId);
    const foundCinema = await cinema.findByPk(cid);

    if (!foundCinema) {
        return res.status(400).json({ msg: "Invalid data!" });
    }

    if (!foundMovie) {
        return res.status(400).json({ msg: "Invalid data!" });
    }

    req.book = foundMovie;

    const isValidTime = foundMovie.showtime_list;

    const now = new Date();

    // // Function to check if the selected time is in the past
    // const isPastTime = (time) => {
    //     const [selectedHour, selectedMinute] = time.split(':').map(Number);
    //     const currentHour = now.getHours();
    //     const currentMinute = now.getMinutes();
    //     return now.toDateString() === d && (currentHour > selectedHour || (currentHour === selectedHour && currentMinute > selectedMinute));
    // };

    // if (!isValidTime.includes(t) || isPastTime(t)) {
    //    return res.status(400).json({ msg: "Invalid data!" });
    // }

    if (foundMovie.start_date > now.toLocaleDateString('en-CA') || now.toLocaleDateString('en-CA') >= foundMovie.expired) {
        return res.status(400).json({ msg: "Invalid data!" });
    }

    const dateTime = `${d}T${t}`; // Combine date and time from seatPick.datepick and seatPick.showtime
    const dateTimeInUTC = new Date(dateTime); // Convert to full date format (example: 2024-04-27T12:53:58.126Z)

    if (isNaN(dateTimeInUTC) || now > dateTimeInUTC || !isValidTime.includes(t)) { // Invalid data if type is not yyyy-dd-mm etc
        return res.status(400).json({ msg: "Invalid data!" });
    }

    await seat
        .findAll({
            where: {
                movie_id: movieId,
                cinema_id: cid,
                datepick: d,
                showtime: t,
            },
            include: [
                {
                    model: book,
                    where: {
                        [Op.or]: [
                            { payment_status: "Issued" },
                            { payment_status: "Pending" }
                        ]
                    }
                }
            ]
        })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

module.exports = {
    checkSeat
};
