const tf = require('@tensorflow/tfjs-node');
const xlsx = require('xlsx');
const natural = require('natural');
const stopword = require('stopword');
const diacritics = require('diacritics');

// Inisialisasi objek kosong
let genres = [];
let genreTicketCounts = {};
let maxTicketsSold = 0;
let avgTicketsPerDay = 0;
let model;

// Fungsi preprocessing dan typo correction (sama seperti sebelumnya)
const handleRepeatedChars = (text) => text.replace(/(.)\1+/g, '$1');
const tokenize = (text) => new natural.WordTokenizer().tokenize(text);
const removeStopWords = (tokens) => stopword.removeStopwords(tokens);
const normalize = (text) => text.toLowerCase();
const removeDiacritics = (text) => diacritics.remove(text);

const preprocessText = (text) => {
    if (!text) return [];
    text = handleRepeatedChars(text);
    text = normalize(text);
    text = removeDiacritics(text);
    let tokens = tokenize(text);
    tokens = removeStopWords(tokens);
    return tokens;
};

// Fungsi untuk memperbaiki typo menggunakan Levenshtein distance dengan ambang batas
const correctTypo = (input, dictionary, threshold = 3) => {
    let bestMatch = null;
    let bestDistance = Infinity;

    dictionary.forEach(word => {
        const distance = natural.LevenshteinDistance(input, word);
        if (distance < bestDistance) {
            bestDistance = distance;
            bestMatch = word;
        }
    });

    return bestDistance <= threshold ? bestMatch : null;
};

const trainModel = async () => {
    let genreCounts = {};
    
    // Load and preprocess data
    const workbook = xlsx.readFile('lib/movie2023.xlsx');
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const data = xlsx.utils.sheet_to_json(worksheet);

    // Get unique genres and preprocess
    const uniqueGenres = [...new Set(data.flatMap(entry => preprocessText(entry.Genre || '').join(', ').split(', ').map(g => {
        g = g.trim();
        genreCounts[g] = (genreCounts[g] || 0) + 1;
        genreTicketCounts[g] = (genreTicketCounts[g] || 0) + (entry['Tickets Sold'] || 0); // Add ticket count to the genre
        return g;
    })))];
    genres = uniqueGenres;

    // Convert genres to one-hot encoding and scale output
    const genreOneHot = data.map(entry => {
        const tokens = preprocessText(entry.Genre || '').map(g => g.trim().toLowerCase());
        return uniqueGenres.map(g => tokens.includes(g) ? 1 : 0);
    });

    maxTicketsSold = Math.max(...data.map(entry => entry['Tickets Sold'] || 0));

    // Menghitung jumlah hari dengan mempertimbangkan total penjualan tiket
    const totalTicketsSold = data.reduce((sum, entry) => sum + (entry['Tickets Sold'] || 0), 0);
    const totalDays = data.length * 28; // Menggunakan asumsi total hari adalah 31 hari per film
    avgTicketsPerDay = totalTicketsSold / totalDays;

    const ticketsSold = data.map(entry => (entry['Tickets Sold'] || 0) / maxTicketsSold);

    // Prepare data for LSTM
    const sequenceLength = 5; // Example sequence length
    const inputSequences = [];
    const targetSequences = [];

    for (let i = 0; i < genreOneHot.length - sequenceLength; i++) {
        const seqX = genreOneHot.slice(i, i + sequenceLength);
        const seqY = ticketsSold[i + sequenceLength];
        inputSequences.push(seqX);
        targetSequences.push(seqY);
    }

    const xs = tf.tensor3d(inputSequences, [inputSequences.length, sequenceLength, uniqueGenres.length]);
    const ys = tf.tensor2d(targetSequences, [targetSequences.length, 1]);

    // Define and compile LSTM model
    model = tf.sequential();
    model.add(tf.layers.lstm({
        units: 50,
        inputShape: [sequenceLength, uniqueGenres.length],
        returnSequences: true
    }));
    model.add(tf.layers.dropout({ rate: 0.2 }));
    model.add(tf.layers.lstm({
        units: 50
    }));
    model.add(tf.layers.dense({ units: 1 }));

    model.compile({ loss: 'meanSquaredError', optimizer: 'adam' });

    // Implement early stopping
    const earlyStoppingCallback = tf.callbacks.earlyStopping({
        monitor: 'val_loss',
        patience: 10
    });

    // Train model
    await model.fit(xs, ys, {
        epochs: 200,
        validationSplit: 0.2,
        callbacks: [earlyStoppingCallback]
    });

    // Save the model
    await model.save('file://./tensorflow');
    
    console.log("Genres: ", genres);
    console.log("genreCounts: ", genreCounts);
    console.log("genreTicketCounts: ", genreTicketCounts); // Log the ticket counts for each genre
    console.log("maxTicketsSold: ", maxTicketsSold);
    console.log("avgTicketsPerDay: ", avgTicketsPerDay);

    return { genres, model, genreCounts, genreTicketCounts, maxTicketsSold, avgTicketsPerDay };
};

const testModel = async () => {
    if (!model || genres.length === 0 || maxTicketsSold === 0) {
        throw new Error("Model has not been trained or initialized properly.");
    }

    // Load and preprocess data
    const workbook = xlsx.readFile('lib/movie2023.xlsx');
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];
    const data = xlsx.utils.sheet_to_json(worksheet);

    const uniqueGenres = genres;

    // Convert genres to one-hot encoding and normalize tickets sold
    const genreOneHot = data.map(entry => {
        const tokens = preprocessText(entry.Genre || '').map(g => g.trim().toLowerCase());
        return uniqueGenres.map(g => tokens.includes(g) ? 1 : 0);
    });

    const ticketsSold = data.map(entry => (entry['Tickets Sold'] || 0) / maxTicketsSold);

    // Prepare data for testing
    const sequenceLength = 5; // Example sequence length
    const inputSequences = [];
    const actualValues = [];

    for (let i = 0; i < genreOneHot.length - sequenceLength; i++) {
        const seqX = genreOneHot.slice(i, i + sequenceLength);
        const seqY = ticketsSold[i + sequenceLength];
        inputSequences.push(seqX);
        actualValues.push(seqY);
    }

    const xs = tf.tensor3d(inputSequences, [inputSequences.length, sequenceLength, uniqueGenres.length]);

    // Load the model
    const predictions = model.predict(xs).dataSync();

    // Evaluate the model
    let correctPredictions = 0;
    actualValues.forEach((actual, index) => {
        const predicted = predictions[index];
        if (Math.abs(predicted - actual) < 0.1) { // Example threshold for considering a prediction "correct"
            correctPredictions++;
        }
    });

    const accuracy = (correctPredictions / actualValues.length) * 100;
    
    console.log("Correct predictions: ", correctPredictions);
    console.log("Total data tested: ", actualValues.length);
    console.log(`Model Accuracy: ${accuracy.toFixed(2)}%`);
    
    return {
        accuracy: accuracy.toFixed(2),
        correctPredictions,
        totalDataTested: actualValues.length,
        actualValues
    };
};

module.exports = {
    testModel,
    trainModel,
    preprocessText,
    correctTypo,
    getModel: () => model,
    getGenres: () => genres,
    getMaxTicketsSold: () => maxTicketsSold,
    getAvgTicketsPerDay: () => avgTicketsPerDay
};

// // Fully Connected Neural Network architecture
// const trainModel = async () => {
//     // Load and preprocess data
//     const workbook = xlsx.readFile('lib/movie2023.xlsx');
//     const worksheet = workbook.Sheets[workbook.SheetNames[0]];
//     const data = xlsx.utils.sheet_to_json(worksheet);

//     // Get unique genres and preprocess
//     const uniqueGenres = [...new Set(data.flatMap(entry => preprocessText(entry.Genre || '').join(', ').split(', ').map(g => {
//         g = g.trim();
//         genreCounts[g] = (genreCounts[g] || 0) + 1;
//         genreTicketCounts[g] = (genreTicketCounts[g] || 0) + (entry['Tickets Sold'] || 0); // Add ticket count to the genre
//         return g;
//     })))];
//     genres = uniqueGenres;

//     // Convert genres to one-hot encoding and scale output
//     const genreOneHot = data.map(entry => {
//         const tokens = preprocessText(entry.Genre || '').map(g => g.trim().toLowerCase());
//         return uniqueGenres.map(g => tokens.includes(g) ? 1 : 0);
//     });

//     maxTicketsSold = Math.max(...data.map(entry => entry['Tickets Sold'] || 0));

//     // Menghitung jumlah hari dengan mempertimbangkan total penjualan tiket
//     const totalTicketsSold = data.reduce((sum, entry) => sum + (entry['Tickets Sold'] || 0), 0);
//     const totalDays = data.length * 31; // Menggunakan asumsi total hari adalah 31 hari per film
//     avgTicketsPerDay = totalTicketsSold / totalDays;

//     const ticketsSold = data.map(entry => (entry['Tickets Sold'] || 0) / maxTicketsSold);

//     // Define and compile model
//     model = tf.sequential();
//     model.add(tf.layers.dense({ units: 50, inputShape: [uniqueGenres.length], activation: 'relu' }));
//     model.add(tf.layers.dropout({ rate: 0.2 }));
//     model.add(tf.layers.dense({ units: 1 }));

//     model.compile({ loss: 'meanSquaredError', optimizer: 'adam' });

//     // Train model
//     const xs = tf.tensor2d(genreOneHot, [genreOneHot.length, uniqueGenres.length]);
//     const ys = tf.tensor2d(ticketsSold, [ticketsSold.length, 1]);

//     // Implement early stopping
//     const earlyStoppingCallback = tf.callbacks.earlyStopping({
//         monitor: 'val_loss',
//         patience: 10
//     });

//     await model.fit(xs, ys, {
//         epochs: 200,
//         validationSplit: 0.2,
//         callbacks: [earlyStoppingCallback]
//     });

//     // Save the model
//     await model.save('file://./tensorflow');
    
//     console.log("Genres: ", genres);
//     console.log("genreCounts: ", genreCounts);
//     console.log("genreTicketCounts: ", genreTicketCounts); // Log the ticket counts for each genre
//     console.log("maxTicketsSold: ", maxTicketsSold);
//     console.log("avgTicketsPerDay: ", avgTicketsPerDay);

//     return { genres, model, genreCounts, genreTicketCounts, maxTicketsSold, avgTicketsPerDay };
// };
