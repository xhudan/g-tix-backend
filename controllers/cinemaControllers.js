const { cinema } = require("../models");
const catchAsync = require("../utils/catchAsync");

const getCinemas = catchAsync(async (req, res) => {
    await cinema
        .findAll({ where: { operate: true } })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getAllCinemas = catchAsync(async (req, res) => {
    await cinema
        .findAll()
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const addCinema = catchAsync(async (req, res) => {
    const { cinema_name, location, operate } = req.body;

    if (!cinema_name.trim() && !location.trim()) {
        return res.status(400).json({ msg: "Missing or invalid variable!" });
    }

    await cinema
        .create({
            cinema_name: cinema_name.trim() === "" ? null : cinema_name.trim(),
            location: location.trim() === "" ? null : location.trim(),
            operate: operate || false })
        .then((data) => res.status(200).json({ msg: "Cinema has been created successfully", data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const updateCinema = catchAsync(async (req, res) => {
    const { cinemaId } = req.params;
    const { cinema_name, location, operate } = req.body;

    if (!cinema_name.trim() && !location.trim()) {
        return res.status(400).json({ msg: "Missing or invalid variable!" });
    }

    await cinema
        .update({
            cinema_name: cinema_name.trim() === "" ? null : cinema_name.trim(),
            location: location.trim() === "" ? null : location.trim(),
            operate
        }, { where: { id: cinemaId }, returning: true }) // menambahkan opsi "returning: true" untuk mendapatkan data yang diperbarui
        .then(([rowsUpdated, [updatedCinema]]) => { // menggunakan destructuring untuk mendapatkan jumlah baris yang diperbarui dan data yang diperbarui
            if (rowsUpdated === 0) {
                // jika tidak ada baris yang diperbarui, kirimkan respons yang sesuai
                res.status(404).json({ msg: "Cinema not found" });
            } else {
                // jika ada baris yang diperbarui, kirimkan respons dengan data yang diperbarui
                res.status(200).json({ msg: "Cinema has been updated successfully", data: updatedCinema });
            }
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const deleteCinema = catchAsync(async (req, res) => {
    const { cinemaId } = req.params;

    cinema
        .destroy({ where: { id: cinemaId } })
        .then(rowsDeleted => {
            if (rowsDeleted === 0) {
                res.status(404).json({ msg: "Cinema not found" });
            } else {
                res.status(200).json({ msg: "Cinema has been deleted successfully" });
            }
        })
        .catch(err => res.status(err.statusCode || 500).json({ msg: err.message }));
});


module.exports = {
    getCinemas,
    getAllCinemas,
    addCinema,
    updateCinema,
    deleteCinema
};
