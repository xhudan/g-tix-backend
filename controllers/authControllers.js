const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const catchAsync = require("../utils/catchAsync");
const { user } = require("../models");
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const { OAuth2Client } = require('google-auth-library');

const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD,
    },
});

let verificationTokens = {}; // In-memory store for tokens

// Function to check whether the password reset token already exists
const isResetTokenExists = (emailReceived) => {
    const existingToken = Object.values(verificationTokens).find(tokenData => tokenData.email === emailReceived);
    return existingToken !== undefined;
};

const sendOTP = catchAsync(async (emailReceived, type, req) => {
    const verificationToken = crypto.randomBytes(32).toString('hex');
    const verificationExpires = Date.now() + 15 * 60 * 1000;

    // Store token and expiration in memory
    verificationTokens[verificationToken] = { email: emailReceived, expires: verificationExpires };

    // Schedule token removal
    setTimeout(() => {
        delete verificationTokens[verificationToken];
    }, 900000);

    const verificationUrl = `${process.env.HOST}/verify?${type}=${verificationToken}`;

    const mailOptions = {
        to: emailReceived,
        // from: "donotreply@gtix.co.id",
        subject: `[G-TIX] ${type === "account" ? "Email Verification" : "Password Reset"}`,
        html:
            `<table role="presentation"
            style="width: 100%; border-collapse: collapse; border: 0px; border-spacing: 0px; font-family: Arial, Helvetica, sans-serif; background-color: rgb(239, 239, 239);"
        >
            <tbody>
                <tr>
                    <td align="center" style="padding: 1rem 2rem; vertical-align: top; width: 100%;">
                        <table role="presentation" style="max-width: 600px; border-collapse: collapse; border: 0px; border-spacing: 0px; text-align: left;">
                            <tbody>
                                <tr>
                                    <td style="padding: 40px 0px 0px;">
                                        <div style="padding: 20px; background-color: rgb(255, 255, 255);">
                                            <div style="color: rgb(0, 0, 0); text-align: left;">
                                                <h1 style="padding-bottom: 16px">Verification Link</h1>
                                                <p>${type === "account" ? "Please verify your email" : "Reset your password"} by clicking the following link:</p>
                                                <a href="${verificationUrl}">${verificationUrl}</a>
                                                <p style="padding-bottom: 16px">If you didn’t request this, please ignore this email.</p>
                                                <p style="padding-bottom: 16px">
                                                    Thank you,<br>
                                                    G-Tix
                                                </p>
                                            </div>
                                        </div>
                                        <div style="padding-top: 20px; color: rgb(153, 153, 153); text-align: center;">
                                            <p style="padding-bottom: 16px">Made with ♥ in Indonesia</p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>`
    };

    await transporter.sendMail(mailOptions);
});

const resendAccountOTP = catchAsync(async (req, res) => {
    const { email } = req.body;
    const emailReceived = email.toLowerCase();

    try {
        if (isResetTokenExists(emailReceived)) {
            return res.status(400).json({ msg: "Email has been sent!" });
        }

        await sendOTP(emailReceived, "account", req);
        res.status(200).json({ msg: `Please check your email for verification` });
    } catch (err) {
        if (err.response && err.response.data && err.response.data.msg) {
            return res.status(500).json({ msg: "Failed to send OTP for reset password. Please try again later." });
        }
        res.status(err.statusCode || 500).json({ msg: err.message });
    }
});

const login = catchAsync(async (req, res) => {
    const { email, password } = req.body;
    const emailReceived = email.toLowerCase();

    await user
        .findOne({ where: { email: emailReceived } })
        .then(async (user) => {
            if (!user) return res.status(400).json({ msg: "Email or password is incorrect!" });

            const validPassword = await bcrypt.compare(password, user.password);
            if (!validPassword) return res.status(400).json({ msg: "Email or password is incorrect!" });

            // Check if the account is active
            if (!user.active) return res.status(400).json({ msg: "Account is not activated" });

            const token = jwt.sign({ id: user.id, email: user.email, full_name: user.full_name, role: user.role },
                process.env.SECRET_KEY, { expiresIn: '1h' }); // Token expires in 1 hour
            res.status(200).json({ token, user: { id: user.id, email: user.email, full_name: user.full_name, role: user.role } });
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const verifyOTP = catchAsync(async (req, res) => {
    const { account, reset } = req.query;
    const { password } = req.body;

    const tokenKey = account || reset;

    if (!verificationTokens[tokenKey]) {
        return res.status(400).json({ msg: 'Verification link is invalid or has expired.' });
    }

    if (verificationTokens[tokenKey].expires < Date.now()) {
        delete verificationTokens[tokenKey];
        return res.status(400).json({ msg: 'Verification link is invalid or has expired.' });
    }

    const userInstance = await user.findOne({ where: { email: verificationTokens[tokenKey].email } });
    if (!userInstance) {
        return res.status(400).json({ msg: 'User not found.' });
    }

    if (account) {
        userInstance.active = true;
        await userInstance.save();
        delete verificationTokens[tokenKey];
        return res.status(200).json({ msg: 'Email successfully verified. You can log in now.' });
    }

    if (!password && reset) {
        return res.status(200).json({ msg: 'Token is valid' });
    }

    if (password && reset) {
        const hashedPassword = await bcrypt.hash(password, 10);
        userInstance.password = hashedPassword;
        await userInstance.save();
        delete verificationTokens[tokenKey];
        return res.status(200).json({ msg: 'Password reset was successful. You can log in now.' });
    }

    return res.status(500).json({ msg: 'An error occurred. Please try again.' });
});

const resetPassword = catchAsync(async (req, res) => {
    const { email } = req.body;
    const emailReceived = email.toLowerCase();

    const userInstance = await user.findOne({ where: { email: emailReceived } });
    if (!userInstance) {
        return res.status(400).json({ msg: 'Email not registered!' });
    }
    if (userInstance.active === false) {
        return res.status(400).json({ msg: 'Verify your email first!' });
    }

    const existingToken = Object.values(verificationTokens).find(tokenData => tokenData.email === emailReceived);
    if (existingToken) {
        // Token still exists, do not send OTP again
        return res.status(400).json({ msg: "Email has been sent!" });
    }

    try {
        if (isResetTokenExists(emailReceived)) {
            return res.status(400).json({ msg: "Email has been sent!" });
        }

        await sendOTP(emailReceived, "reset", req);
        res.status(200).json({ msg: "Please check your email for reset password" });
    } catch (err) {
        if (err.response && err.response.data && err.response.data.msg) {
            return res.status(500).json({ msg: "Failed to send OTP for reset password. Please try again later." });
        }
        res.status(err.statusCode || 500).json({ msg: err.message });
    }
});

const register = catchAsync(async (req, res) => {
    const { full_name, email, password, role, confirm_password } = req.body;
    const emailReceived = email.toLowerCase();
    const inputRole = role || "User";

    // Whitelist email domain check
    const allowedEmailDomains = [
        "gmail.com", "yahoo.com", "outlook.com", "hotmail.com", "live.com",
        "icloud.com", "aol.com", "protonmail.com", "zoho.com", "mail.com",
        "gmx.com", "yandex.com", "me.com", "fastmail.com", "hushmail.com", "tutanota.com"
    ];
    const emailDomain = emailReceived.split('@')[1];
    if (!allowedEmailDomains.includes(emailDomain)) {
        return res.status(400).json({ msg: "Email domain is not allowed!" });
    }

    if (password !== confirm_password) {
        return res.status(400).json({ msg: "Password and confirm password don't match!" });
    }

    // Password validation with a minimum of 8 characters, at least one letter and one number, may include symbols including spaces but is not mandatory
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d).{8,}$/;
    if (!passwordRegex.test(password)) {
        return res.status(400).json({
            msg: "The password is less secure!",
            msgDetail: "New password must contain at least 8 characters with at least one letter and one number"
        });
    }

    const existingUser = await user.findOne({ where: { email: emailReceived } });
    if (existingUser) return res.status(400).json({ msg: "Email already exists" });

    const hashedPassword = await bcrypt.hash(password, 10);

    try {
        await user.create({
            full_name: full_name.trim() === "" ? null : full_name.trim(),
            email: emailReceived,
            password: hashedPassword,
            role: inputRole,
            active: false,
        });

        await sendOTP(emailReceived, "account", req);
        res.status(200).json({ msg: "Please check your email for verification" });
    } catch (err) {
        res.status(err.statusCode || 500).json({ msg: err.message })
    }
});

const updateUser = catchAsync(async (req, res) => {
    const { full_name, password, new_password, confirm_new_password } = req.body;

    const currentUser = await user.findByPk(req.user.id);
    if (!currentUser) {
        return res.status(404).json({ msg: "User not found!" });
    }

    const oldToken = req.headers.authorization.split(' ')[1];
    const decodedOldToken = jwt.decode(oldToken, { complete: true });

    // Calculates remaining time in seconds
    const currentTime = Math.floor(Date.now() / 1000);
    const expiresInOldToken = decodedOldToken.payload.exp - currentTime;

    let hashedPassword;

    if (password || new_password || confirm_new_password) {
        if (currentUser.password !== null) {
            const validPassword = await bcrypt.compare(password, currentUser.password);
            if (!validPassword) {
                return res.status(400).json({ msg: "Current password is incorrect!" });
            }
        }

        if (new_password !== confirm_new_password) {
            return res.status(400).json({ msg: "New password and confirm password don't match!" });
        }

        // Validate 8 character numbers and letters
        const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
        if (!passwordRegex.test(new_password)) {
            return res.status(400).json({
                msg: "The new password is less secure!",
                msgDetail: "New password must contain at least 8 characters with at least one letter and one number"
            });
        }

        hashedPassword = await bcrypt.hash(new_password, 10);
    }

    const updateData = {};
    if (full_name !== undefined) {
        updateData.full_name = full_name.trim() === "" ? null : full_name.trim();
    }
    if (hashedPassword) {
        updateData.password = hashedPassword;
    }

    await currentUser.update(updateData)
        .then(() => {
            const token = jwt.sign(
                { id: req.user.id, email: req.user.email, full_name: updateData.full_name || req.user.full_name, role: req.user.role },
                process.env.SECRET_KEY,
                { expiresIn: expiresInOldToken }
            );
            res.status(200).json({ msg: "User updated successfully", token });
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const checkPassword = catchAsync(async (req, res) => {
    const currentUser = await user.findByPk(req.user.id);
    if (!currentUser) {
        return res.status(404).json({ msg: "User not found!" });
    }

    res.status(200).json({ isPassword: Boolean(currentUser.password) });
});

// test users
const getAllUsers = catchAsync(async (req, res) => {
    await user
        .findAll({
            attributes: ['id', 'email', 'full_name', 'role', 'active']
        })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const googleCallback = async (req, res) => {
    try {
        const token = req.body.token;

        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: process.env.GOOGLE_CLIENT_ID,
        });
        const payload = ticket.getPayload();
        const { email, name } = payload;

        // Check if the user exists
        let existingUser = await user.findOne({ where: { email } });

        if (!existingUser) {
            // Create new user if not found
            existingUser = await user.create({
                full_name: name,
                email,
                role: "User",
                active: true,
            });
        }

        // Generate JWT token
        const jwtToken = jwt.sign({
            id: existingUser.id,
            email: existingUser.email,
            full_name: existingUser.full_name,
            role: existingUser.role,
        }, process.env.SECRET_KEY, { expiresIn: '1h' });

        // Respond with token
        res.status(200).json({ token: jwtToken, user: { id: existingUser.id, email: existingUser.email, full_name: existingUser.full_name, role: existingUser.role } });
    } catch (err) {
        console.error("Error in Google callback:", err);
        res.status(500).json({ msg: "Internal server error" });
    }
};

module.exports = {
    login,
    register,
    getAllUsers,
    updateUser,
    checkPassword,
    resetPassword,
    verifyOTP,
    resendAccountOTP,
    googleCallback
};
