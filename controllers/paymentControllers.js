const { book, seat } = require("../models/");
const cron = require("node-cron");
const catchAsync = require("../utils/catchAsync");
const midtransClient = require('midtrans-client');

let snap = new midtransClient.Snap({
    isProduction: false,
    serverKey: process.env.MIDTRANS_SERVER_KEY,
    clientKey: process.env.MIDTRANS_CLIENT_KEY
});

const makePayment = catchAsync(async (req, res) => {
    const { code } = req.params;

    const foundBook = await book.findOne({
        where: { booking_code: code }, include: [
            { model: seat },
        ]
    });

    if (!foundBook) {
        return res.status(400).json({ msg: "Invalid booking code" });
    }

    req.book = foundBook;

    if (foundBook.payment_status !== "Pending") {
        return res.status(400).json({ msg: "Payment is not available" });
    }

    if (foundBook.midtrans_link !== null) {
        res.status(200).json({ message: "Continue payment", redirectUrl: foundBook.midtrans_link });
    } else {
        const dateTime = `${foundBook.seats[0].datepick}T${foundBook.seats[0].showtime}`; // Combine date and time from seat.datepick and seat.showtime
        const dateTimeInUTC = new Date(dateTime); // Convert to full date format (example: 2024-04-27T12:53:58.126Z)

        const defaultDeadline = new Date(foundBook.createdAt.getTime() + 15 * 60000); // 60 seconds * 1000 milliseconds

        const getDefaultDeadline = ~~((defaultDeadline - new Date()) / 60000);
        // let deadline;

        // if (defaultDeadline > dateTimeInUTC) {
        //     // Math.floor(-100 / 3) // -34,
        //     // ~~(-100 / 3)         // -33,
        //     deadline = ~~((dateTimeInUTC - foundBook.createdAt) / 60000);
        // } else {
        //     deadline = 15;
        // };

        const deadline = defaultDeadline > dateTimeInUTC ? ~~((dateTimeInUTC - foundBook.createdAt) / 60000) : getDefaultDeadline;
 
        if (deadline < 5) {
            return res.status(400).json({ msg: "An error occurred", msgDetail: "An error occurred or you can only pay 5 minutes before the film shows" });
        }

        const transaction_details = {
            order_id: code,
            gross_amount: foundBook.total_price,
        };

        const page_expiry = {
            duration: deadline,
            unit: "minute",
        };

        const expiry = {
            duration: deadline,
            unit: "minute",
        };

        const callbacks = {
            finish: `${process.env.HOST}/history`,
        }

        const parameter = {
            transaction_details,
            page_expiry,
            expiry,
            callbacks
        };

        let redirectUrl;

        snap.createTransaction(parameter)
            .then(async (transaction) => {
                redirectUrl = transaction.redirect_url;
                res.status(200).json({ message: "The payment has been successfully made", redirectUrl });
            })
            .catch((err) => {
                res.status(err.statusCode || 500).json({ msg: "An error occurred", msgDetail: "An error occurred or you can only pay 5 minutes before the film shows" });
            });

        const cronRunner = cron.schedule("* * * * * *", async () => { // running every second, * * * * * for every minute
            const booking = await book.findByPk(foundBook.id, {
                attributes: ['payment_status']
            });

            const now = new Date();
            const getDeadline = new Date(foundBook.createdAt.getTime() + deadline * 60000);

            let statusResponse;
            try {
                statusResponse = await snap.transaction.status(foundBook.booking_code);
            } catch (error) {
                statusResponse = null;
            }

            if (!foundBook.midtrans_link && statusResponse) {
                await foundBook.update({ midtrans_link: redirectUrl });
            }

            if (booking.payment_status === "Pending" && statusResponse) {
                if (statusResponse.transaction_status === "settlement" || statusResponse.transaction_status === "capture") {
                    await foundBook.update({ payment_status: "Issued" });
                    cronRunner.stop();
                } else if (statusResponse.transaction_status === "expire" || statusResponse.transaction_status === "cancel" || statusResponse.transaction_status === "deny") {
                    // await seat.update({ seat_number: null }, { where: { booking_id: foundBook.id } });
                    await foundBook.update({ payment_status: "Cancelled" });
                    cronRunner.stop();
                }
            } else if (booking.payment_status === "Cancelled" && statusResponse) {
                await snap.transaction.cancel(foundBook.booking_code);
                cronRunner.stop();
            } else if (now > getDeadline && !statusResponse) {
                cronRunner.stop();
            }
        });
    }
});

module.exports = {
    makePayment
}
