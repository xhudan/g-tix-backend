const { book, movie, cinema, seat } = require("../models/");
const catchAsync = require("../utils/catchAsync");

const historyAllBooking = catchAsync(async (req, res) => {
    await book
        .findAll({
            include: [
                { model: seat },
                { model: movie },
                { model: cinema },
            ]
        })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const historyBooking = catchAsync(async (req, res) => {
    await book
        .findAll({
            where: { user_id: req.user.id }, include: [
                { model: seat },
                { model: movie },
                { model: cinema },
            ]
        })
        .then((data) => res.status(200).json({ data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const cancelBooking = catchAsync(async (req, res) => {
    const { bookingId } = req.params;

    await book
            .update({ payment_status: "Cancelled" }, { where: { id: bookingId } })
            .then(rowsDeleted => {
                if (rowsDeleted === 0) {
                    res.status(404).json({ msg: "Booking not found" });
                } else {
                    res.status(200).json({ msg: "Booking successfully cancelled" });
                }})
            .catch(err => res.status(err.statusCode || 500).json({ msg: err.message }));
});

// const cancelBooking = catchAsync(async (req, res) => {
//     const { bookingId } = req.params;

//     try {
//         // Delete seats
//         const seatResult = await seat.update({ seat_number: null }, { where: { booking_id: bookingId } });

//         // Delete book
//         const bookResult = await book.update({ payment_status: "Cancelled" }, { where: { id: bookingId } });

//         if (seatResult && bookResult) {
//             res.status(200).json({ msg: "Order successfully cancelled" });
//         } else if (seatResult) {
//             res.status(200).json({ msg: "Seat reservations cancelled" });
//         } else {
//             res.status(404).json({ msg: "Order not found" });
//         }
//     } catch (err) {
//         res.status(err.statusCode || 500).json({ msg: err.message });
//     }
// });

const addBooking = catchAsync(async (req, res) => {
    const generateCode = (length) => {
        let code = "";
        while (code.length < length) {
            let char = Math.random().toString(36).substr(2, 1);
            if (Math.random() < 0.5) {
                char = char.toUpperCase();
            }
            code += char;
        }
        return code;
    };

    const { movieId } = req.params;
    const { q, cid, d, t } = req.query;
    const { seats } = req.body;

    const movies = await movie.findByPk(movieId);
    const cinemas = await cinema.findByPk(cid);
    const quantity = parseInt(q);

    let bookingCode;

    const isCodeExisting = async (code) => {
        const uniqueCode = await book.findOne({
            where: { booking_code: code },
        });
        return !uniqueCode;
    };

    do {
        bookingCode = generateCode(8);
    } while (!(await isCodeExisting(bookingCode)));

    try {
        const newBooking = await book.create({
            cinema_id: cinemas.id,
            movie_id: movies.id,
            total_booking: quantity,
            total_price: parseInt(quantity * movies.price),
            booking_code: bookingCode,
            payment_status: "Pending",
            user_id: req.user.id,
        });

        const seatPick = await seat.bulkCreate(
            seats.map((seatIndex) => ({
                cinema_id: cinemas.id,
                movie_id: movies.id,
                seat_number: seatIndex.seat_number,
                booking_id: newBooking.id,
                datepick: d,
                showtime: t,
            }))
        );

        const dateTime = `${seatPick[0].datepick}T${seatPick[0].showtime}`; // Combine date and time from seatPick.datepick and seatPick.showtime
        const dateTimeInUTC = new Date(dateTime); // Convert to full date format (example: 2024-04-27T12:53:58.126Z)

        const defaultDeadline = new Date(newBooking.createdAt.getTime() + 15 * 60000); // 60 seconds * 1000 milliseconds

        // Convert to timestamp and reverse to date to get real time
        const rushedDeadline = dateTimeInUTC - newBooking.createdAt;

        const deadline = defaultDeadline > dateTimeInUTC ? rushedDeadline : 900000;

        setTimeout(async () => {
            const booking = await book.findByPk(newBooking.id, {
                attributes: ['id', 'payment_status']
            });
            if (booking.payment_status === "Pending") {
                // await seat.update({ seat_number: null }, { where: { booking_id: booking.id } });
                await booking.update({ payment_status: "Cancelled" });
            }
        }, deadline);

        res.status(200).json({
            // msg: "The booking has been successfully made.\nPlease complete the payment within 15 minutes or before the film airs!",
            msg: "Please complete the payment within 15 minutes or before the film airs!",
            data: { seatPick, newBooking },
        });
    } catch (error) {
        res.status(error.statusCode || 500).json({ msg: error.message });
    }
});

module.exports = {
    addBooking,
    cancelBooking,
    historyBooking,
    historyAllBooking
};

        // const defaultDeadline = new Date(newBooking.createdAt.getTime() + 2 * 60000); // 60 seconds * 1000 milliseconds
        
        // // Convert to timestamp and reverse to date to get real time
        // const rushedDeadline = new Date().getTime() + (dateTimeInUTC - newBooking.createdAt);
        // const getDateRushedDeadline = new Date(rushedDeadline);
        
        // let deadline;
        
        // if (defaultDeadline > dateTimeInUTC) {
        //     deadline = getDateRushedDeadline;
        // } else {
        //     deadline = defaultDeadline;
        // }
        
        // // Create a cron time pattern based on deadline's time
        // const cronPattern = `${deadline.getMinutes()} ${deadline.getHours()} ${deadline.getDate()} ${deadline.getMonth() + 1} *`; // Month index starts from 0 (January) to 11 (December)
        
        // const scheduledJob = cron.schedule(cronPattern, async () => { // Run once at minute 15
        //     const booking = await book.findByPk(newBooking.id, {
        //         attributes: ['id', 'payment_status']
        //     });
        //     if (booking.payment_status === "Pending") {
        //         // await seat.update({ seat_number: null }, { where: { booking_id: booking.id } });
        //         await booking.update({ payment_status: "Cancelled" });
        //     }
        //     scheduledJob.stop();
        // });
