const express = require("express");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");
const { sequelize: db } = require("./models/index.js");
const cors = require("cors");
// const { trainModel } = require("./controllers/trainModel.js");

const app = express();
const PORT = process.env.PORT;

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan("dev"));
app.use(cookieParser());

app.get("/", function (req, res) {
  res.send("Running");
});

app.use(require("./routes"));

// running app
db.authenticate()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`The server is running and has successfully connected to the database`);
    });
  })
  .catch((err) => {
    console.log(err);
  });

// // running app with tensorflow
// db.authenticate()
//   .then(() => {
//     // Train the model after database authentication
//     trainModel()
//       .then(({ genres, writers, directors, actorsList, languages }) => {
//         console.log("Movie model has been trained successfully");
//         // Start the server after model training
//         app.listen(PORT, () => {
//           console.log(`The server is running and has successfully connected to the database`);
//         });
//       })
//       .catch((error) => {
//         console.error("Error training movie model:", error);
//       });
//   })
//   .catch((err) => {
//     console.error("Error authenticating database:", err);
//   });
