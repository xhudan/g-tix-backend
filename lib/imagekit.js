const ImageKit = require("imagekit");

const { PUBLIC_KEY, URL_ENDPOINT, PRIVATE_KEY } = process.env;

let imagekit = new ImageKit({
  publicKey: PUBLIC_KEY,
  urlEndpoint: URL_ENDPOINT,
  privateKey: PRIVATE_KEY,
});

const uploadToImagekit = async (request) => {
  const file = request.file;
  const ext = file.originalname.split(".").pop();

  const img = await imagekit.upload({
    file: file.buffer,
    fileName: `GTIX_${new Date().toLocaleDateString("en-CA")}.${ext}`,
  });
  return img;
};

module.exports = { uploadToImagekit };
