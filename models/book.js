'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class book extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.cinema, { foreignKey: "cinema_id" });
      this.belongsTo(models.movie, { foreignKey: "movie_id" });
      this.hasMany(models.seat, { foreignKey: "booking_id" });
      this.belongsTo(models.user, { foreignKey: "user_id" });
    }
  }
  book.init({
    cinema_id: DataTypes.INTEGER,
    movie_id: DataTypes.INTEGER,
    total_booking: DataTypes.INTEGER,
    total_price: DataTypes.DOUBLE,
    booking_code: DataTypes.STRING,
    payment_status: DataTypes.ENUM('Issued', 'Pending', 'Cancelled'),
    user_id: DataTypes.INTEGER,
    midtrans_link: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'book',
  });
  return book;
};