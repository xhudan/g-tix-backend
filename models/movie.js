'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.seat, { foreignKey: "cinema_id" });
      this.hasMany(models.book, { foreignKey: "cinema_id" });
    }
  }
  movie.init({
    poster: DataTypes.TEXT,
    title: DataTypes.STRING,
    director: DataTypes.ARRAY(DataTypes.STRING),
    writer: DataTypes.ARRAY(DataTypes.STRING),
    actors: DataTypes.ARRAY(DataTypes.TEXT),
    language: DataTypes.STRING,
    duration: DataTypes.TIME,
    age: DataTypes.INTEGER,
    genre: DataTypes.ARRAY(DataTypes.STRING),
    synopsis: DataTypes.TEXT,
    showtime_list: DataTypes.ARRAY(DataTypes.TIME),
    start_date: DataTypes.DATEONLY,
    expired: DataTypes.DATEONLY,
    price: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'movie',
  });
  return movie;
};