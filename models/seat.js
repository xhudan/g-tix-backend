'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class seat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.cinema, { foreignKey: "cinema_id" });
      this.belongsTo(models.movie, { foreignKey: "movie_id" });
      this.belongsTo(models.book, { foreignKey: "booking_id" });
    }
  }
  seat.init({
    cinema_id: DataTypes.INTEGER,
    movie_id: DataTypes.INTEGER,
    seat_number: DataTypes.INTEGER,
    booking_id: DataTypes.INTEGER,
    datepick: DataTypes.DATEONLY,
    showtime: DataTypes.TIME,
  }, {
    sequelize,
    modelName: 'seat',
  });
  return seat;
};