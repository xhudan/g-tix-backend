'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cinema extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.seat, { foreignKey: "cinema_id" });
      this.hasMany(models.book, { foreignKey: "cinema_id" });
    }
  }
  cinema.init({
    cinema_name: DataTypes.STRING,
    location: DataTypes.TEXT,
    operate: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'cinema',
  });
  return cinema;
};