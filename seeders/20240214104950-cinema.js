'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('cinemas', [
      {
        cinema_name: "Bekasi",
        location: "Kalimalang, Bekasi",
        operate: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        cinema_name: "Jakarta",
        location: "Salemba, Jakarta",
        operate: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        cinema_name: "Depok",
        location: "Margonda, Depok",
        operate: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        cinema_name: "Tangerang",
        location: "Karawaci, Tangerang",
        operate: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        cinema_name: "IKN",
        location: "Ibu Kota Nusantara",
        operate: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('cinemas', null, {});
  }
};
