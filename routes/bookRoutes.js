const router = require("express").Router();
const { historyBooking, addBooking, cancelBooking, historyAllBooking } = require("../controllers/bookControllers");

// middleware
const Auth = require("../middleware/auth");

// API
router.get("", Auth.verifiedUser, historyBooking);
router.post("/:movieId", Auth.verifiedUser, addBooking);
router.delete("/:bookingId", Auth.verifiedUser, cancelBooking);
router.get("/all", Auth.verifiedUser, Auth.isAdmin, historyAllBooking);

module.exports = router;
