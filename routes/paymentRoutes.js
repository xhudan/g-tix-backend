const router = require("express").Router();
const { makePayment } = require("../controllers/paymentControllers");

// middleware
const Auth = require("../middleware/auth");

// API
router.post("/:code", Auth.verifiedUser, makePayment);

module.exports = router;
