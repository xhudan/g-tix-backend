const router = require("express").Router();
const { checkSeat } = require("../controllers/seatControllers");

// middleware
const Auth = require("../middleware/auth");

// API
router.get("/:movieId", Auth.verifiedUser, checkSeat);

module.exports = router;
