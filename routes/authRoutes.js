const router = require("express").Router();

const {
  login,
  register,
  getAllUsers,
  updateUser,
  checkPassword,
  resetPassword,
  verifyOTP,
  resendAccountOTP,
  googleCallback
} = require("../controllers/authControllers.js");

// middleware
const Auth = require("../middleware/auth.js");

// for get all users debugging
router.get("/users", Auth.verifiedUser, Auth.isAdmin, getAllUsers);

// API
router.post("/login", login);
router.post("/register", register);
router.post("/reset-password", resetPassword);
router.post("/verify", verifyOTP);
router.post("/send-OTP", resendAccountOTP);
router.put("/update-user", Auth.verifiedUser, updateUser);
router.get("/check-password", Auth.verifiedUser, checkPassword);
router.post("/google/callback", googleCallback);

router.post("/verify-token", Auth.verifiedUser, (req, res) => {
  res.status(200).json({ msg: "Token is valid" });
});

module.exports = router;
