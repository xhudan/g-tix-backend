const router = require("express").Router();
const { getAllMovies, getMovies, getMovieByPK, addMovie, updateMovie, deleteMovie, getRecommendationMovies } = require("../controllers/movieControllers");
const { trainModel, testModel } = require("../controllers/trainModel")

// middleware
const Auth = require("../middleware/auth");
const upload = require("../middleware/uploader");

// API
router.get("", getMovies);
router.post("", Auth.verifiedUser, Auth.isAdmin, upload, addMovie);
router.get("/all", Auth.verifiedUser, Auth.isAdmin, getAllMovies);
router.get("/recommendations", Auth.verifiedUser, getRecommendationMovies);
router.get("/trainModel", Auth.verifiedUser, Auth.isAdmin, async (req, res) => {
    try {
        const { genreCounts,  genreTicketCounts, maxTicketsSold, avgTicketsPerDay } = await trainModel();
        res.status(200).json({
            msg: "Tensorflow model has been trained successfully",
            genreCounts,
            genreTicketCounts,
            maxTicketsSold,
            avgTicketsPerDay
        });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
});
router.get("/testModel", Auth.verifiedUser, Auth.isAdmin, async (req, res) => {
    try {
        const { accuracy, correctPredictions, totalDataTested, actualValues } = await testModel();
        res.status(200).json({
            msg: "TensorFlow model has been tested successfully",
            accuracy: `${accuracy}%`,
            correctPredictions,
            totalDataTested,
            actualValues
        });
    } catch (error) {
        res.status(500).json({ msg: error.message });
    }
});
router.get("/:movieId", getMovieByPK);
router.put("/:movieId", Auth.verifiedUser, Auth.isAdmin, upload, updateMovie);
router.delete("/:movieId", Auth.verifiedUser, Auth.isAdmin, deleteMovie);

module.exports = router;
