const router = require("express").Router();
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../docs/swagger.json");
const authRoutes = require("./authRoutes");
const bookRoutes = require("./bookRoutes");
const cinemaRoutes = require("./cinemaRoutes");
const seatRoutes = require("./seatRoutes");
const movieRoutes = require("./movieRoutes");
const paymentRoutes = require("./paymentRoutes");

//Open API
router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// API
router.use("/api/v1/auth", authRoutes);
router.use("/api/v1/booking", bookRoutes);
router.use("/api/v1/cinema", cinemaRoutes);
router.use("/api/v1/checkseat", seatRoutes);
router.use("/api/v1/movie", movieRoutes);
router.use("/api/v1/payment", paymentRoutes);

module.exports = router;
