const router = require("express").Router();
const { getCinemas, getAllCinemas, addCinema, updateCinema, deleteCinema } = require("../controllers/cinemaControllers");

// middleware
const Auth = require("../middleware/auth");

// API
router.get("", getCinemas);
router.post("", Auth.verifiedUser, Auth.isAdmin, addCinema);
router.put("/:cinemaId", Auth.verifiedUser, Auth.isAdmin, updateCinema);
router.delete("/:cinemaId", Auth.verifiedUser, Auth.isAdmin, deleteCinema);
router.get("/all", Auth.verifiedUser, Auth.isAdmin, getAllCinemas);

module.exports = router;
