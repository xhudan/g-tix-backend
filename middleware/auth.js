const jwt = require("jsonwebtoken");
const catchAsync = require("../utils/catchAsync");
const secret = process.env.SECRET_KEY;

const verifiedUser = catchAsync(async (req, res, next) => {
  const token = req.headers.authorization;
  if (!token || token === null) {
    return res.status(403).json({ msg: "Unauthorized request" });
  }

  const splitToken = token.split(" ")[1];
  
  try {
    let verifiedUser = jwt.verify(splitToken, secret);
    if (!verifiedUser) {
      return res.status(403).json({ msg: "Access denied" });
    }
    req.user = verifiedUser;
    next();
  } catch (error) {
    return res.status(403).json({ msg: "Invalid token" });
  }
});

const isAdmin = catchAsync(async (req, res, next) => {
  if (req.user.role === "Admin") {
    next();
  } else {
    return res.status(403).json({ msg: "Access denied" });
  }
});

module.exports = { verifiedUser, isAdmin };
