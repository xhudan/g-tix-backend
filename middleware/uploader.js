const multer = require("multer");

const filter = (req, file, cb) => {
  if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
    cb(null, true);
  } else {
    cb(new Error("Only PNG, JPG, and JPEG image files are allowed."), false);
  }
};

const upload = multer({
  fileFilter: filter,
}).single("poster");

module.exports = upload;